/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NUM;

/**
 *
 * @author RedS
 */
public class BigInteger {

    private static final BigInteger bigMaxInt = BigInteger.valueOf(Integer.MAX_VALUE);
    private static final BigInteger bigMinInt = BigInteger.valueOf(Integer.MIN_VALUE);

    public static BigInteger intRangeCheck(BigInteger val) {
        if (val.compareTo(bigMaxInt) == 1 || val.compareTo(bigMinInt) == -1) {
            throw new ArithmeticException("Integer overflow");
        }
        return val;
    }

    public static int multAccum(int oldAcc, int newVal, int scale) {
        BigInteger product = BigInteger.valueOf(newVal).multiply(BigInteger.valueOf(scale));
        BigInteger res = intRangeCheck(BigInteger.valueOf(oldAcc).add(product));
        return res.intValue(); // Safe conversion 
    }
