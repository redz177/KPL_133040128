/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NUM;

import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author RedS
 */
public class GetInteger {

    public static long getInteger(DataInputStream is) throws IOException {
        return is.readInt() & 0xFFFFFFFFL; // Mask with 32 one-bits }
    }
}