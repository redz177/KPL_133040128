/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NUM;

/**
 *
 * @author RedS
 */
public class FloatingPoint {

    double currentBalance; // User's cash balance   

    void doDeposit(String userInput) {
        double val = 0;
        try {
            val = Double.valueOf(userInput);
        } catch (NumberFormatException e) {
// Handle input format error   
        }
        if (Double.isInfinite(val)) {
// Handle infinity error   
        }
        if (Double.isNaN(val)) {
// Handle NaN error   
        }
        if (val >= Double.MAX_VALUE - currentBalance) {
// Handle range error   
        }
        currentBalance += val;
    }
}
