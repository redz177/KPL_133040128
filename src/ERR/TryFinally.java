/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ERR;

/**
 *
 * @author RedS
 */
public class TryFinally {

    private static boolean doLogic() {
        try {
            throw new IllegalStateException();
        } finally {
            System.out.println("logic done");
        }     // Any return statements must go here;     
            // applicable only when exception is thrown conditionally   
    }
}
