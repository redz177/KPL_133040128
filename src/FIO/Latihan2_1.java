/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FIO;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

/**
 *
 * @author RedS
 */
public class Latihan2_1 {
    String filename = /* Provided by user */ ;
    Path path = new File(filename).toPath();

    
        try {
            if (!isInSecureDir(path)) {
            System.out.println("File not in secure directory");
            return;
        }

        BasicFileAttributes attr = Files.readAttributes(
                path, BasicFileAttributes.class, LinkOption.NOFOLLOW_LINKS);

        // Check
        if (!attr.isRegularFile()) {
            System.out.println("Not a regular file");
            return;
        }
        // Other necessary checks

        try (InputStream in = Files.newInputStream(path)) {
            // Read file
        }
    }
    catch (IOException x) {
  // Handle error
    }
}
