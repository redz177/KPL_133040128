/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FIO;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author RedS
 */
public class Latihan2_8 {
    // Read method
public static int readLittleEndianInteger(InputStream ips)
            throws IOException {
        byte[] buffer = new byte[4];
        int check = ips.read(buffer);

        if (check != 4) {
            throw new IOException("Unexpected End of Stream");
        }

        int result = (buffer[3] << 24) | (buffer[2] << 16)
                | (buffer[1] << 8) | buffer[0];
        return result;
    }

// Write method
    public static void writeLittleEndianInteger(int i, OutputStream ops)
            throws IOException {
        byte[] buffer = new byte[4];
        buffer[0] = (byte) i;
        buffer[1] = (byte) (i >> 8);
        buffer[2] = (byte) (i >> 16);
        buffer[3] = (byte) (i >> 24);
        ops.write(buffer);
    }
}
