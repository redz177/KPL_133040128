/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FIO;

/**
 *
 * @author RedS
 */
public class Latihan2_5 {
    public final class InputLibrary {

        private static BufferedInputStream in
                = new BufferedInputStream(System.in);

        static BufferedInputStream getBufferedWrapper() {
            return in;
        }

        // ... Other methods
    }

// Some code that requires user input from System.in
    class AppCode {

        private static BufferedInputStream in;

        AppCode() {
            in = InputLibrary.getBufferedWrapper();
        }

        // ... Other methods
    }
}
