/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FIO;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author RedS
 */
public class Latihan2_4 {

    try (FileInputStream stream = new FileInputStream(fileName);
    BufferedReader bufRead= new BufferedReader(new InputStreamReader(stream))) {
 
        String line;
        while ((line = bufRead.readLine()) != null) {
            sendLine(line);
        }
    }
    catch (IOException e) {
  // Forward to handler
    }
}
