/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FIO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author RedS
 */
public class Latihan2_9 {
    public class Hook {

        public static void main(String[] args) {
            try {
                final InputStream in = new FileInputStream("file");
                Runtime.getRuntime().addShutdownHook(new Thread() {
                    public void run() {
                        // Log shutdown and close all resources
                        in.close();
                    }
                });

                // ...
            } catch (IOException x) {
                // Handle error
            } catch (FileNotFoundException x) {
                // Handle error
            }
        }
    }
}
