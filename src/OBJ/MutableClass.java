/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OBJ;

import java.util.Date;

/**
 *
 * @author RedS
 */
public class MutableClass {

    private final Date date;

    public MutableClass(MutableClass mc) {
        this.date = new Date(mc.date.getTime());
    }

    public MutableClass(Date d) {
        this.date = new Date(d.getTime());  // Make defensive copy   
    }

    public Date getDate() {
        return (Date) date.clone(); // Copy and return   

    }
}

