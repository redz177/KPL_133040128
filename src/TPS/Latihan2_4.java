/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPS;

/**
 *
 * @author RedS
 */
public class Latihan2_4 {
    final class PoolService {

        private final ExecutorService pool = Executors.newFixedThreadPool(10);

        public void doSomething() {
            Future<?> future = pool.submit(new Task());

            // ...
            try {
                future.get();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt(); // Reset interrupted status
            } catch (ExecutionException e) {
                Throwable exception = e.getCause();
                // Forward to exception reporter
            }
        }
    }
}
