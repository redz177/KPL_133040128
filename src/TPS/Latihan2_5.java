/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPS;

/**
 *
 * @author RedS
 */
public class Latihan2_5 {
    class CustomThreadPoolExecutor extends ThreadPoolExecutor {

        public CustomThreadPoolExecutor(int corePoolSize,
                int maximumPoolSize, long keepAliveTime,
                TimeUnit unit, BlockingQueue<Runnable> workQueue) {
            super(corePoolSize, maximumPoolSize, keepAliveTime,
                    unit, workQueue);
        }

        @Override
        public void beforeExecute(Thread t, Runnable r) {
            if (t == null || r == null) {
                throw new NullPointerException();
            }
            Diary.setDay(Day.MONDAY);
            super.beforeExecute(t, r);
        }
    }

    public final class DiaryPool {
        // ...

        DiaryPool() {
            exec = new CustomThreadPoolExecutor(NumOfthreads, NumOfthreads,
                    10, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10));
            diary = new Diary();
        }
        // ...
    }
}
