/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPS;

/**
 *
 * @author RedS
 */
public class Latihan2_3 {
    public final class SocketReader implements Runnable {

        private final SocketChannel sc;
        private final Object lock = new Object();

        public SocketReader(String host, int port) throws IOException {
            sc = SocketChannel.open(new InetSocketAddress(host, port));
        }

        @Override
        public void run() {
            ByteBuffer buf = ByteBuffer.allocate(1024);
            try {
                synchronized (lock) {
                    while (!Thread.interrupted()) {
                        sc.read(buf);
                        // ...
                    }
                }
            } catch (IOException ie) {
                // Forward to handler
            }
        }
    }

    public final class PoolService {
        // ...
    }
}
