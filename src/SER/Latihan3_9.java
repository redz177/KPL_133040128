/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SER;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author RedS
 */
public class Latihan3_9 {
    public static void main(String[] args) throws IOException {
        ObjectOutputStream out = null;
        try {
          out = new ObjectOutputStream(
              new BufferedOutputStream(new FileOutputStream("ser.dat")));
          while (SensorData.isAvailable()) {
            // Note that each SensorData object is 1 MB in size
            SensorData sd = SensorData.readSensorData();
            out.writeObject(sd);
            out.reset(); // Reset the stream
          }
        } finally {
          if (out != null) {
            out.close();
          }
        }
      }
}
