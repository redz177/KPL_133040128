/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SER;

/**
 *
 * @author RedS
 */
public class Latihan3_11 {
    public Set whitelist;
 
    public WhitelistedObjectInputStream(InputStream inputStream, Set wl) throws IOException {
      super(inputStream);
      whitelist = wl;
    }

    @Override
    protected Class<?> resolveClass(ObjectStreamClass cls) throws IOException, ClassNotFoundException {
      if (!whitelist.contains(cls.getName())) {
        throw new InvalidClassException("Unexpected serialized class", cls.getName());
      }
      return super.resolveClass(cls);
    }
  }

  class DeserializeExample {
    private static Object deserialize(byte[] buffer) throws IOException, ClassNotFoundException {
      Object ret = null;
      Set whitelist = new HashSet<String>(Arrays.asList(new String[]{"GoodClass1","GoodClass2"}));
      try (ByteArrayInputStream bais = new ByteArrayInputStream(buffer)) {
        try (WhitelistedObjectInputStream ois = new WhitelistedObjectInputStream(bais, whitelist)) {
          ret = ois.readObject();
        }
      }
      return ret;
    }
  }
}
