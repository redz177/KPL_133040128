/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SER;

/**
 *
 * @author RedS
 */
public class Latihan3_7 {
    private transient int ticket = 1;
    private transient SecureRandom draw = new SecureRandom();

    public Lottery(int ticket) {
      this.ticket = (int) (Math.abs(ticket % 20000) + 1);
    }

    public final int getTicket() {
      return this.ticket;
    }

    public final int roll() {
      this.ticket = (int) ((Math.abs(draw.nextInt()) % 20000) + 1);
      return this.ticket;
    }

    public static void main(String[] args) {
      Lottery l = new Lottery(2);
      for (int i = 0; i < 10; i++) {
        l.roll();
        System.out.println(l.getTicket());
      }
    }

    private void readObject(ObjectInputStream in)
            throws IOException, ClassNotFoundException {
      in.defaultReadObject();
      this.draw = new SecureRandom();
      roll();
    }
}
