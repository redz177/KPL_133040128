/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MET;

/**
 *
 * @author RedS
 */
public class ReadSensitive {

    public final void readSensitiveFile() {
        try {
            SecurityManager sm = System.getSecurityManager();
            if (sm != null) {  // Check for permission to read file       
                sm.checkRead("/temp/tempFile");
            }     // Access the file   
        } catch (SecurityException se) {     // Log exception   
        }
    }
}
