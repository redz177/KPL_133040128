/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TSM;

/**
 *
 * @author RedS
 */
public class Latihan3_3 {
    public final class ConnectionFactory {

        private static final ThreadLocal<Connection> connectionHolder
                = new ThreadLocal<Connection>() {
            @Override
            public Connection initialValue() {
                try {
                    Connection dbConnection
                            = DriverManager.getConnection("connection string");
                    return dbConnection;
                } catch (SQLException e) {
                    return null;
                }
            }
        };

        // Other fields ...
        static {
            // Other initialization (do not start any threads)
        }

        public static Connection getConnection() {
            Connection connection = connectionHolder.get();
            if (connection == null) {
                throw new IllegalStateException("Error initializing connection");
            }
            return connection;
        }

        public static void main(String[] args) {
            // ...
            Connection connection = getConnection();
        }
    }
}
