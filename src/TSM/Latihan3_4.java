/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TSM;

/**
 *
 * @author RedS
 */
public class Latihan3_4 {
    class Foo {

        private volatile Helper helper;

        public Helper getHelper() {
            return helper;
        }

        public void initialize() {
            helper = new Helper(42);
        }
    }

// Mutable but thread-safe Helper
    public class Helper {

        private volatile int n;
        private final Object lock = new Object();

        public Helper(int n) {
            this.n = n;

        }

        public void setN(int value) {
            synchronized (lock) {
                n = value;
            }
        }
    }
}
