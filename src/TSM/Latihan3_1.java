/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TSM;

/**
 *
 * @author RedS
 */
public class Latihan3_1 {
    class Base {

        private final Object lock = new Object();

        public void doSomething() {
            synchronized (lock) {
                // ...
            }
        }
    }

    class Derived extends Base {

        Logger logger
                = // Initialize
 
        private final Object lock = new Object();

        @Override
        public void doSomething() {
            synchronized (lock) {
                try {
                    super.doSomething();
                } finally {
                    logger.log(Level.FINE, "Did something");
                }
            }
        }
    }
}
