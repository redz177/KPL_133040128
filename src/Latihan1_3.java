/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author RedS
 */
public class Latihan1_3 {
    public final class ProcessStep implements Runnable {

        private static final Object lock = new Object();
        private static int time = 0;
        private final int step; // Perform operations when field time
        // reaches this value

        public ProcessStep(int step) {
            this.step = step;
        }

        @Override
        public void run() {
            try {
                synchronized (lock) {
                    while (time != step) {
                        lock.wait();
                    }

                    // Perform operations
                    time++;
                    lock.notifyAll(); // Use notifyAll() instead of notify()
                }
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt(); // Reset interrupted status
            }
        }

    }
}
