package DCL;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author RedS
 */
public class Cycle1 {

    private final int balance;
// Random Deposit   
    private static final int deposit = (int) (Math.random() * 100);
// Inserted after initialization of required fields   
    private static final Cycle1 c = new Cycle1();

    public Cycle1() {
// subtract processing fee     
        balance = deposit - 10;
    }

    public static void main(String[] args) {
        System.out.println("The account balance is: " + c.balance);
    }
}
