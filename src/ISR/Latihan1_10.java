/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ISR;

import java.io.BufferedOutputStream;
import java.io.IOException;

/**
 *
 * @author RedS
 */
public class Latihan1_10 {
      private static void createXMLStream(final BufferedOutputStream outStream,
        final String quantity) throws IOException, NumberFormatException {
        // Write XML string only if quantity is an unsigned integer (count).
        int count = Integer.parseUnsignedInt(quantity);
        String xmlString = "<item>\n<description>Widget</description>\n"
                + "<price>500</price>\n" + "<quantity>" + count + "</quantity></item>";
        outStream.write(xmlString.getBytes());
        outStream.flush();
    }
}
