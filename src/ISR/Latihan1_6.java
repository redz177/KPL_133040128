/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ISR;

import java.io.File;

/**
 *
 * @author RedS
 */
public class Latihan1_6 {
    class DirList {

        public static void main(String[] args) throws Exception {
            File dir = new File(System.getProperty("dir"));
            if (!dir.isDirectory()) {
                System.out.println("Not a directory");
            } else {
                for (String file : dir.list()) {
                    System.out.println(file);
                }
            }
        }
    }
}
